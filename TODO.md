**Hooks à ajouter :**
* `hyphenopoly.js` ;

**Hooks à améliorer :**
* `facing-sections.js` : renuméroter correctement les pages, ajouter une numérotation spécifique à ces sections qui se font face, et ainsi se passer de `after-folio.js`.

**UX :**
* penser d'autres styles pour la correction (demande d'avis ? doute ?) ;

**Avec Caroline :**
* Ajouter une version "par défaut" sur certaines propositions alternatives.

* Réparer la prévisualisation des backups ?