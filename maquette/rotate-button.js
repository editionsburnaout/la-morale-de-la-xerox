const claraLink = "./index.html";
const florianLink = "./florian.html";

class RotateButton extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    beforeParsed(content) {
        console.log("overlay is working...");

        let pannel = document.createElement("aside");
        pannel.setAttribute("data-id", "overlay")
        pannel.style = "position: fixed;"; // otherwise pagedjs removes the position attribute

        // Is it Clara's side of Florian's ?
        let claraOrFlorian = content.querySelectorAll("section")[0].id == 'couv_florian';

        var rotate = document.createElement("a");
        rotate.innerHTML = "Le côté<br />de " + (claraOrFlorian?"Clara":"Florian") + " &#x21BA;";
        rotate.href = claraOrFlorian?claraLink:florianLink;
        pannel.appendChild(rotate);
        document.querySelector("body").appendChild(pannel);
    }
}
Paged.registerHandlers(RotateButton);