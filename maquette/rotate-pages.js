const claraFolio = "C"; // this will be appended after there folios
const florianFolio = "F";

class RotatePages extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    afterRendered(pages) {
        var leftPages = document.querySelectorAll('.pagedjs_left_page');
        var rightPages = document.querySelectorAll('.pagedjs_right_page');

        // There we rotate the pages
        leftPages.forEach(page => {
            page.querySelectorAll('.pagedjs_pagebox').forEach(pagebox => {
                pagebox.style.transform = "rotate(180deg)" ;
                console.log("pagebox");
            })
        })
        
        // There we reverse the order
        // La logique qu'on peut adopter est que chaque page de gauche, par définition, va avoir une page de droite qui la précède
        // On trouve chacun de ces couple : pageDroitePrec - pageGauche
        // On fait une file pour les pages de gauche et une pile pour les pages de droite
        // On défile/dépile en associant les nouveaux couples

        let leftPagesArr = Array.prototype.slice.call(leftPages);
        let rightPagesArr = Array.prototype.slice.call(rightPages);

        // Until this point, we didn't care about the number of pages, but there they must be equal.

        // Variables to figure out right the folios
        let nbPages = rightPagesArr.length-2;
        let c = 0; // first page number, 0 is the cover
        let claraOrFlorian = leftPages[1].classList.contains("pagedjs_voice1_page");

        while(rightPagesArr){
            try {
                let currentLPage = leftPagesArr.pop();
                let currentRPage = rightPagesArr.shift();

                // we can update the page numbers here.
                currentLPage.querySelectorAll(".folio")[0].innerHTML = (nbPages - c)+(claraOrFlorian?claraFolio:florianFolio);
                currentRPage.querySelectorAll(".folio")[0].innerHTML = (c++)+(claraOrFlorian?florianFolio:claraFolio);

                currentRPage.insertAdjacentElement(`afterend`, currentLPage);
            } catch(e) {
                console.error("There are not as many left pages as they are right pages");
                break;
            }
        }

        // dunno why there is a last white page popping up...
        document.querySelectorAll('.pagedjs_pages')[0].lastChild.remove();

        // reverse ids margins afterwards... hehehe that's cheating but it works !!
        document.querySelectorAll(".marginal-text").forEach( margin => {
            if(margin.getAttribute("data-id") == "voice1_margins") {
                margin.setAttribute("data-id", "voice2_margins") ;
            } else margin.setAttribute("data-id", "voice1_margins") ;
        });
    }
}


Paged.registerHandlers(RotatePages);