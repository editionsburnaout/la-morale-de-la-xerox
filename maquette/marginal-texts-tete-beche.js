/**
 * @file Pagedjs' hook that adds content into the margins of a pagedjs_page element after render.
 * @author Yann Trividic
 * @license GPLv3
 * @see https://gitlab.com/the-moral-of-the-xerox-vf
 */

 class marginalTexts extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    afterRendered(pages) {
      let claraOfFlorian = pages[0].element.classList.contains("pagedjs_couv_voice1_page");
      console.log("claraOfFlorian", claraOfFlorian);
      console.log("pages !", pages[0].element.classList);
      console.log("marginalTexts is working...");
      
      var margins1 = document.querySelector('#voice1_margins').children;
      var pagesClara = document.querySelectorAll('.pagedjs_voice1_page');
      // console.log("clara_margins", margins1);
      
      var margins2 = document.querySelector('#voice2_margins').children;
      var pagesFlorian = document.querySelectorAll('.pagedjs_voice2_page');
      // console.log("florian_margins", margins2);

      // console.log("Pages Clara Forian ", pagesClara, pagesFlorian)

      var counterMargins1 = 0;
      var counterMargins2 = 0;

      for(let i = 0; i < pagesFlorian.length; i++){
        this.increment(pagesClara[i], margins1, counterMargins1, 1, "voice1_margins"); // Clara
        if ((i == 0)) {
          this.increment(pagesFlorian[i], margins2, counterMargins2, 1, "voice2_margins"); // First page Florian
        }
        else {
          console.log((margins2.length-2-counterMargins2));
          this.increment(pagesFlorian[i], margins2, margins2.length-2-counterMargins2, 2, "voice2_margins"); // Florian
        }
        // console.log("page " + (i+1) + " done.");
      }
    }

    increment(page, marginTexts, counter, nbTexts, id) {  
      var marginTextBox = document.createElement("div");
      marginTextBox.setAttribute("class", "margin-text-box");
    
      for(let j = 0; j < nbTexts; j++){
        var marginContent = marginTexts.item(counter);
    
        //console.log("marginContent", marginContent);
        try {
          marginContent.classList.add("marginal-text");
          marginContent.setAttribute("data-id", id);
          marginTextBox.insertAdjacentElement(`beforeend`, marginContent);
        } catch (error) {
          //console.error("There are less margin texts than there are pages.");
        }
      }
      counter++;
      let marginRight = page.querySelectorAll(".pagedjs_margin-right")[0];
      marginRight.insertAdjacentElement(`beforeend`, marginTextBox);
    }
  }
  Paged.registerHandlers(marginalTexts);