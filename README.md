# La Morale de la Xerox

Ce dépôt vise à archiver le travail de traduction de l'anglais vers le français de _The Morale of the Xerox_, un texte de [Clara Lobregat Balaguer](https://hardworkinggoodlooking.com/) et de [Florian Cramer](http://floriancramer.nl) publié en 2017 à l'occasion du festival [_Pluriversale VII: Stealing from the West_](https://www.adkdw.org/en/article/1196_pluriversale_vii) de l'[Akademie der Künste der Welt](https://www.adkdw.org). Cette traduction collective sera publiée à l'automne par les [Éditions Burn~Août](https://editionsburnaout.fr/).

## Description

De mars à juin 2023, avec les Éditions Burn~Août, nous avons organisé une série d'ateliers dans l'objectif de traduire collectivement _The Morale of the Xerox_, un texte de Clara Lobregat Balaguer et de Floriant Cramer. Pour l'occasion de ces ateliers, nous nous sommes dit qu'il serait pratique de pouvoir s'appuyer sur un logiciel qui faciliterait la traduction collective. Le voici dans sa version éprouvée pour _La Morale de la Xerox_. Le code de cette webapp est ici un peu crado, il a été nettoyé pour en faire un vrai programme que nous proposons sous licence libre : [Padatrad](https://gitlab.com/editionsburnaout/padatrad).

Une copie de ce dépôt est hébergée en ligne. Pour voir ce que ça donne dans un navigateur, c'est par ici : https://padatrad.editionsburnaout.fr/morale. Pour les fichiers finaux envoyés à l'imprimeur, c'est par là : https://morale.editionsburnaout.fr

![Vue du logiciel de traduction](https://gitlab.com/yanntrividic/the-moral-of-the-xerox-vf/-/raw/main/morale.png?ref_type=heads)

## Auteurices et remerciements

Merci à la [Galerie municipale Jean-Collet](https://galerie.vitry94.fr) de Vitry-sur-Seine qui a financé cette traduction et apporté un lieu pour les ateliers (et merci en particulier à [Daniel Purroy](https://dpurroy.wixsite.com) !), à [Antoine Lefebvre](http://www.antoinelefebvre.net/) pour nous avoir partagé l'existence de cetexte, à Clara et Florian, autrice et auteur de _The Moral of the Xerox_, à [Roman Seban](https://www.bureauromanseban.fr) et à l'[Association Presse Offset](http://associationpresseoffset.fr/) pour le graphisme, la maquette et l'impression [à venir]. Merci à [Julien Taquet](https://twitter.com/John_Tax) et à [Nicolas Taffin](https://polylogue.org/) pour l'aide avec PagedJS. Et surtout, merci à tous les participants et participantes aux ateliers : Julien, Caroline D., Yaëlle, France, Patrice, Palmyre, Nathalie, Taton, Agathe, Signe, Laetitia, Mathilde, Richard, Lucie, Anne et Caroline R.

Ce logiciel a été développé et est maintenu par Yann Trividic pour le compte des Éditions Burn~Août.

## Licences

Traduction : CC BY-NC-SA 4.0. \
Texte original : Clara Balaguer et Florian Cramer. \
Padatrad : GNU-GPL3. \
Hooks PagedJS originaux : GNU-GPL3.