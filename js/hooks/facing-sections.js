/**
 * @file Pagedjs' hook that reorganizes pages after render 
 * so that one section is on the left pages, and another one on the right pages.
 * The hook deals with an uneven amount of pages by adding blank pages where needed.
 * Page numbers are updated so they match with the new layout.
 * 
 * /!\ Warning: this hook breaks the pagedjs folios.
 * /!\ They are replaced by adding independant pagination numbers.
 * 
 * @author Yann Trividic
 * @license GPLv3
 * @see https://gitlab.com/the-moral-of-the-xerox-vf
 */

class FacingSections extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    afterRendered(pages) {
      console.log("FacingSections is working...");

      var pagesVoice1 = document.querySelectorAll('.pagedjs_voice1_page, .pagedjs_couv_voice1_page');
      var pagesVoice2 = document.querySelectorAll('.pagedjs_voice2_page, .pagedjs_couv_voice2_page');

      const nbPages = Math.max(pagesVoice1.length, pagesVoice2.length);

      const pageNumberFirstFacingPage = this.getPageNumberFirstFacingPage(pagesVoice1);

      // add n blank pages at the end of our document
      const pagesToAdd = Math.abs(pagesVoice1.length - pagesVoice2.length);
      // we need it if we have an uneven amount of pages between the two sections
      for(let i = 0; i < pagesToAdd; i++){
        this.chunker.addPage(true);
      } 

      for(let i = 0; i < nbPages; i++){
        let leftPage = pagesVoice1.item(i);
        let leftPageNumber = pageNumberFirstFacingPage + (2*i) ;
        let rightPage = pagesVoice2.item(i);
        let rightPageNumber = leftPageNumber + 1 ;

        if(leftPage != null){ // if the page exists
          this.changePageNumber(leftPage, leftPageNumber);
        } else { // otherwise we create it
          this.insertBlankPage(rightPage, true, leftPageNumber);
        }

        if(rightPage != null){
          this.changePageNumber(rightPage, rightPageNumber);
          if(leftPage != null) leftPage.insertAdjacentElement(`afterend`, rightPage);
        } else {
          this.insertBlankPage(leftPage, false, rightPageNumber);
        }
      }

      // If pages were added, then we need to update the page number of
      // all the following pages.
      if(pagesToAdd){
        const pages = document.querySelectorAll('.pagedjs_page');
        for(let i = this.getPageNumberLastFacingPage(pagesVoice1, pagesVoice2); i < pages.length; i++) {
          this.changePageNumber(pages.item(i), i + 1); // the +1 is here because we start counting from the previous page
        }
      }

      console.log("Now replacing the folios...");
  
      var allPages = document.querySelectorAll('.pagedjs_margin-bottom-right-corner-holder');

      for(let i = 1 ; i < allPages.length ; i++){ // the last page is not taken into account
          const folio = document.createElement("div");
          folio.setAttribute("class", "folio");
          folio.innerText = i;
          allPages.item(i-1).insertAdjacentElement(`beforeend`, folio);
      }
    }

    /**
     * Makes sure the pagedjs_left_page class is applied to this page
     * @param {pagedjs_page} page 
     */
    makePageLeft(page) {
      page.classList.remove('pagedjs_right_page');
      page.classList.add('pagedjs_left_page'); // we make sure it is a left_page=
    }
    

    /**
     * Makes sure the pagedjs_right_page class is applied to this page
     * @param {pagedjs_page element} page
     */
    makePageRight(page) {
      page.classList.remove('pagedjs_left_page');
      page.classList.add('pagedjs_right_page');
    }

    /**
     * Inserts a blank page in the event that there is an uneven amount of facing pages
     * @param {pagedjs_page element} page The page element around which will be added the blank page 
     * @param {Boolean} addLeftPage If true, the page added will be a left page
     * @param {Integer} pageNumber The page number of the new blank page added
     */
    insertBlankPage(page, addLeftPage, pageNumber){
      let newPage = this.catchBlankPageAdded();
      this.changePageNumber(newPage, pageNumber);
      if(addLeftPage){
        page.insertAdjacentElement(`beforebegin`, newPage);
      } else {
        page.insertAdjacentElement(`afterend`, newPage);
      }
    }

    /**
     * In this script, we are generating the pages we will need to add at the beginning,
     * so we need to go get them at the end of the book.
     * Here is how we do it.
     * @returns a blank page
     */
    catchBlankPageAdded(){
      let page = document.querySelectorAll('.pagedjs_pages').item(0).lastChild;
      // apparently there is an object between the pagedjs_page divs that is not a div
      // we need to squeeze it.
      while(page.tagName != 'DIV'){
        page = page.previousSibling;
      }
      return page;
    }

    /**
     * Returns the page number of the first facing section's page
     * @param {List of elements} pagesVoice1 First facing section
     * @returns First page number
     */
    getPageNumberFirstFacingPage(pagesVoice1){
      return parseInt(pagesVoice1.item(0).getAttribute('data-page-number'));
    }

    /**
     * Returns the page number of the last facing section's page
     * @param {List of elements} pagesVoice1 facing section 1
     * @param {List of elements} pagesVoice2 facing section 2
     * @returns Last page number
     */
     getPageNumberLastFacingPage(pagesVoice1, pagesVoice2){
      const pages = (pagesVoice1.length - pagesVoice2.length > 0 ? pagesVoice1 : pagesVoice2);
      const pageNumber = parseInt(pages.item(pages.length-1).getAttribute('data-page-number'));
      console.log(pageNumber);
      return pageNumber;
    }

    /**
     * Modifies the page number to make it match the new order of pages.
     * Keeps the old page number in a new attribute: old-data-page-number.
     * An even page number will give away a left page, and vice-versa.
     * 
     * @param {pagedjs_page element} page 
     * @param {Integer} pageNumber 
     */
    changePageNumber(page, pageNumber){
      // first, we make sure the page number matches a left or right page
      if(pageNumber % 2) {
        this.makePageRight(page);
      } else {
        this.makePageLeft(page);
      }

      // then we update the page number
      const oldPageNumber = page.getAttribute("data-page-number");
      page.setAttribute("data-page-number", pageNumber);

      // and add an attribute consequently
      page.setAttribute("old-data-page-number", oldPageNumber);
      page.setAttribute("id", "page-" + pageNumber);
    }
  }
  Paged.registerHandlers(FacingSections);