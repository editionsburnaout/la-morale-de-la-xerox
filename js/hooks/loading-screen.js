/**
 * @file Pagedjs' hook that adds an overlay to pagedjs to display content that's not in the print.
 * @author Yann Trividic
 * @license GPLv3
 * @see https://gitlab.com/the-moral-of-the-xerox-vf
 *
 * based on Benoit Launay's forensic.js script
 * @see https://gitlab.coko.foundation/pagedjs/templaters/forensic
 */

class loadingScreen extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
        this.loader = null;
    }

    initLoader(content){
        this.loader = content.querySelector("#loader");
        this.loader.setAttribute("data-id", "loader")
        this.loader.style = "position: fixed;"; // otherwise pagedjs removes the position attribute
    }

    beforeParsed(content) {
        console.log("overlay is working...");
        this.initLoader(content);
        document.body.append(this.loader);
    }

    afterRendered(content) {
        this.loader.remove();
    }
}
Paged.registerHandlers(loadingScreen);
