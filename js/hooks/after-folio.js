/* Pagedjs' hook that adds folio in the corner of a pagedjs_page element after pagedjs has rendered your page.
 * Copyright (C) 2023 Yann Trividic
 * Licence: GPL v3 - https://gitlab.com/the-moral-of-the-xerox-vf
 */

class afterFolio extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    afterRendered(pages) {
      console.log("afterFolio is working...");
  
        var allPages = document.querySelectorAll('.pagedjs_margin-bottom-right-corner-holder');

        for(let i = 1 ; i < allPages.length ; i++){ // the last page is not taken into account
            const folio = document.createElement("div");
            folio.setAttribute("class", "folio");
            folio.innerText = i;
            allPages.item(i-1).insertAdjacentElement(`beforeend`, folio);
        }
    }
  }
  Paged.registerHandlers(afterFolio);