/**
 * @file Pagedjs' hook that adds content into the margins of a pagedjs_page element after render.
 * @author Yann Trividic
 * @license GPLv3
 * @see https://gitlab.com/the-moral-of-the-xerox-vf
 */

class marginalTexts extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    afterRendered(pages) {
      console.log("marginalTexts is working...");
      // console.log("marginal-texts is working.");

      var allPages = document.querySelectorAll('.pagedjs_margin-right');
      // console.log("pages", allPages);

      var margins1 = document.querySelector('#voice1_margins').children;
      // console.log("clara_margins", margins1);

      var margins2 = document.querySelector('#voice2_margins').children;
      // console.log("florian_margins", margins2);

      var counterMargins1 = 0;
      var counterMargins2 = 0;

      for(let i = 1; i < allPages.length; i++){
        if((i%2) == 1) {
          this.increment(i, allPages, margins1, counterMargins1, 1, "voice1_margins"); // Clara
        } 
        else if ((i == 2)) {
          this.increment(i, allPages, margins2, counterMargins2, 1, "voice2_margins"); // First page Florian
        }
        else {
          this.increment(i, allPages, margins2, counterMargins2, 2, "voice2_margins"); // Florian
        }
        // console.log("page " + (i+1) + " done.");
      }
    }

    increment(i, allPages, marginTexts, counter, nbTexts, id) {  
      var marginTextBox = document.createElement("div");
      marginTextBox.setAttribute("class", "margin-text-box");
    
      for(let j = 0; j < nbTexts; j++){
        var marginContent = marginTexts.item(counter);
    
        //console.log("marginContent", marginContent);
        try {
          marginContent.classList.add("marginal-text");
          marginContent.setAttribute("data-id", id);
          marginTextBox.insertAdjacentElement(`beforeend`, marginContent);
        } catch (error) {
          //console.error("There are less margin texts than there are pages.");
        }
      }
      counter++;
      allPages.item(i).insertAdjacentElement(`beforeend`, marginTextBox);
    }
  }
  Paged.registerHandlers(marginalTexts);