// version 3 - 09/2021


// hyphenate function 


// Whitelist : word that can be hyphenated even if there is an initial cap
const whitelist = new Array(

);

// Blacklist : les mots qui seront insécables quoi qu'il arrive word that wont be hyphenated
const blacklist = new Array(

);


class insecables extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  beforeParsed(content) {
    console.log("Starting hyphenation.");
    Object.keys(Hyphenopoly.setup.selectors).forEach(sel => {
      content.querySelectorAll(sel).forEach(elem => {
        Hyphenopoly.hyphenators["HTML"].then((hyn) => {
          hyn(elem, sel);
        });
      });
    });
  }
}

Paged.registerHandlers(insecables);


/* No hyphens between pages */
/* warning : may cause polyfill errors */


class noHyphenBetweenPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.hyphenToken;
  }

  afterPageLayout(pageFragment, page, breakToken) {

    if (pageFragment.querySelector('.pagedjs_hyphen')) {

      // find the hyphenated word  
      let block = pageFragment.querySelector('.pagedjs_hyphen');

      block.dataset.ref = this.prevHyphen;

      // move the breakToken
      let offsetMove = getFinalWord(block.innerHTML).length;

      // move the token accordingly
      page.breakToken = page.endToken.offset - offsetMove;

      // remove the last word
      block.innerHTML = block.innerHTML.replace(getFinalWord(block.innerHTML), "");

      breakToken.offset = page.endToken.offset - offsetMove;

    }
  }

}

Paged.registerHandlers(noHyphenBetweenPage);

function getFinalWord(words) {
  var n = words.split(" ");
  return n[n.length - 1];
}

