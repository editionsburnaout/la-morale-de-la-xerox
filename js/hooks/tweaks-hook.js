

// adds id to each element from that list:
// "figure",
//  "figcaption",
//  "img",
//  "ol",
//  "ul",
//  "li",
//  "p",
//  "img",
//  "table",
//  "h1",
//  "h2",
//  "h3",
//  "div",
//  "aside"
// then allows to use custom properties in hooks.css for identified elements
// page floats and justification tweaks

// the second version of that code should run per chapter to reduce the changes

class addId extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
      console.log("elements with no id will get one");
      addIDtoEachElement(content);
  }
}

Paged.registerHandlers(addId);

function addIDtoEachElement(content) {
  if (document.querySelector('meta[name="chapterid"]')) {
    var chapterID = document.querySelector('meta[name="chapterid"]').content;
    chapterID = chapterID + "_";
  } else {
    var chapterID = "";
  }

  var classtable = [];
  let tags = ["figure", "ol", "ul", "p", "table", "h1", "h2", "h3", "div", "aside"];
  tags.forEach( tag => {
    content.querySelectorAll(tag).forEach((el, index) => {
      var cName = el.className.split(" ")[0];
      classtable.push(cName);
      const countOccurrences = (arr, val) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0);
      if (!el.id) {
        if (cName.length == 0) { 
          var customId = chapterID + el.tagName.toLowerCase() + "_" + index; 
        } else {
          var elementOccurence = countOccurrences(classtable,cName);
          var customId = chapterID  + el.tagName.toLowerCase() + "_" + cName + "_" + elementOccurence;
        }
        el.id = customId;
      }
    });
  })
}

// interpret custom property on tweaks and add the corresponding class

class CSStoClass extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatSameTop = [];
    this.floatSameBottom = [];
    this.floatNextTop = [];
    this.floatNextBottom = [];
    this.spacing = [];
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // page floats
    if (declaration.property == "--experimental-page-float") {
      if (declaration.value.value.includes("same-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameTop.push(sel.split(","));
        //console.log("floatSameTop: ", this.floatSameTop);
      } else if (declaration.value.value.includes("same-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameBottom.push(sel.split(","));
        //console.log("floatSameBottom: ", this.floatSameBottom);
      } else if (declaration.value.value.includes("next-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextTop.push(sel.split(","));
        //console.log('floatNextTop: ', this.floatNextTop);
      } else if (declaration.value.value.includes("next-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextBottom.push(sel.split(","));
        //console.log("floatNextBottom: ", this.floatNextBottom);
      }
    } 
    // spacing
    else if (declaration.property == "--experimental-spacing") {
        var spacingValue = declaration.value.value;
        spacingValue = spacingValue.replace(/\s/g, '');
        spacingValue = parseInt(spacingValue);
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        var thisSpacing = [
          sel.split(","), spacingValue
        ]
        this.spacing.push(thisSpacing);
    }
  }


  afterParsed(parsed) {

    if (this.floatNextBottom) {
      this.floatNextBottom.forEach((elNBlist) => {
        console.log(elNBlist);
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-next-bottom");
          console.log("#" + el.id + " moved to next-bottom");
        });
      });
    }
    if (this.floatNextTop) {
      this.floatNextTop.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-next-top");
          console.log("#" + el.id + " moved to next-top");
        });
      });
    }
    if (this.floatSameTop) {
      this.floatSameTop.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-same-top");
          console.log("#" + el.id + " moved to same-top");
        });
      });
    }
    if (this.floatSameBottom) {
      this.floatSameBottom.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-same-bottom");
          console.log("#" + el.id + " moved to same-bottom");
        });
      });
    }
    if (this.spacing) {
      this.spacing.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist[0]).forEach((el) => {
          var spacingValue = elNBlist[1];
          var spacingClass = "spacing-" + spacingValue;
          console.log(spacingClass);
          el.classList.add(spacingClass);
          console.log("#" + el.id + " spaced " + spacingValue);
        });
      });
    }
  }
}

Paged.registerHandlers(CSStoClass);









// lets you manualy add classes to some pages elements 
// to simulate page floats.
// works only for elements that are not across two pages


let classElemFloatSameTop = "page-float-same-top"; // ← class of floated elements on same page
let classElemFloatSameBottom = "page-float-same-bottom"; // ← class of floated elements bottom on same page

let classElemFloatNextTop = "page-float-next-top"; // ← class of floated elements on next page
let classElemFloatNextBottom = "page-float-next-bottom"; // ← class of floated elements bottom on next page



class elemFloatTop extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.experimentalFloatNextTop = [];
    this.experimentalFloatNextBottom = [];
    this.token;
  }


  layoutNode(node) {
    // If you find a float page element, move it in the array,
    if (node.nodeType == 1 && node.classList.contains(classElemFloatNextTop)) {
      let clone = node.cloneNode(true);
      this.experimentalFloatNextTop.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = "none";
    }
    if (node.nodeType == 1 && node.classList.contains(classElemFloatNextBottom)) {
      let clone = node.cloneNode(true);
      this.experimentalFloatNextBottom.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = "none";
    }
  }

  beforePageLayout(page, content, breakToken) {
    //console.log(breakToken);
    // If there is an element in the floatPageEls array,
    if (this.experimentalFloatNextTop.length >= 1) {
      // Put the first element on the page.
      page.element.querySelector(".pagedjs_page_content").insertAdjacentElement('afterbegin', this.experimentalFloatNextTop[0]);
      this.experimentalFloatNextTop.shift();
    }
    if (this.experimentalFloatNextBottom.length >= 1) {
      // Put the first element on the page.
      page.element.querySelector(".pagedjs_page_content").insertAdjacentElement('afterbegin', this.experimentalFloatNextBottom[0]);
      this.experimentalFloatNextBottom.shift();
    }
  }

  // works only with non breaked elements
  afterPageLayout(page, content, breakToken) {
    // todo -> si il est coupé on annule tout
    if (page.querySelector("." + classElemFloatSameTop)) {
      var bloc = page.querySelector("." + classElemFloatSameTop);
      page.querySelector(".pagedjs_page_content").insertAdjacentElement('afterbegin', bloc);
    }
    // try fixed bottom on same if requested
    if (page.querySelector("." + classElemFloatSameBottom)) {
      var bloc = page.querySelector("." + classElemFloatSameBottom);
      bloc.classList.add("absolute-bottom");
    }
    // try fixed bottom if requested
    if (page.querySelector("." + classElemFloatNextBottom)) {
      var bloc = page.querySelector("." + classElemFloatNextBottom);
      bloc.classList.add("absolute-bottom");
    }
  }

}
Paged.registerHandlers(elemFloatTop);