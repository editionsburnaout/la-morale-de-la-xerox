/**
 * @file Regenerate previous versions of the work out of ZIP files.
 * The only modification needed to add a backup is to add an entry to `backups.json`
 * and add the corresponding ZIP file, generated by clicking on the saveBtn
 * 
 * @author Yann Trividic
 * @license GPLv3
 */

/**
 * Fetches the backups from the backups.json file
 */
async function fetchBackups() {
    const response = await fetch('backups.json');
    const backups = await response.json();
    return backups;
}

/**
 * Generates HTML contents to interact with the backups.json file.
 * It gives three possibilities :
 * 1) Generate a preview of the backup with pagedjs
 * 2) Have a look at the PDF as pages
 * 3) Have a look at the imposed PDF
 */
function getListOfBackups() {
    fetchBackups().then(backups => {
        backups.backups.forEach((backup) => {
            const p = document.createElement("p");
            const date = backup.file_id.substring(18, 20) + "/" + backup.file_id.substring(16, 18) + "/" + backup.file_id.substring(12, 16) ;
            const participants = getParticipants(backup);
            p.innerHTML = backup.string + " (" + date + ") avec " + participants + "&#x00A0;: ";
            document.body.appendChild(p);

            const ul = document.createElement("ul");

            var li = document.createElement("li");
            const aZip = document.createElement("a");
            aZip.setAttribute("href", window.location.href + "backup.html?zip=zip/" + backup.file_id + ".zip");
            aZip.innerHTML = "Prévisualisation&#x202F;;";
            li.appendChild(aZip);
            ul.appendChild(li);

            li = document.createElement("li");
            var aPdf = document.createElement("a");
            aPdf.setAttribute("href", "pdf_pages/" + backup.file_id + ".pdf");
            aPdf.innerHTML = "PDF pages&#x202F;;";
            li.appendChild(aPdf);
            ul.appendChild(li);

            li = document.createElement("li");
            aPdf = document.createElement("a");
            aPdf.setAttribute("href", "pdf_imposition/" + backup.file_id + ".pdf");
            aPdf.innerText = "PDF imposé.";
            li.appendChild(aPdf);
            ul.appendChild(li);

            document.body.appendChild(ul);
        });
    });
}

function getParticipants(backup){
    let s = "" ;
    backup.participants.forEach((p) => {
        s = s + p + ", "
    })
    return s.slice(0, s.length - 2) ;
}

function getZipFromArgs(){
    params = new URLSearchParams(window.location.search);
    return params.get("zip");
}